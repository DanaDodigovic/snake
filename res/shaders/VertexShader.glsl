#version 460 core

out vec3 fragment_color;

layout(location = 0) in vec4 position;
layout(location = 1) in vec3 color_in;

uniform mat4 MVP;

void main()
{
    gl_Position = MVP * position;
    fragment_color = color_in;
}
