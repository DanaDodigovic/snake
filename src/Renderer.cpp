#include "Renderer.hpp"

#include <stdexcept>

Renderer::Renderer(RenderPrimitive primitive) : primitive(primitive) {}

void Renderer::draw(const VertexArray& va,
                    const IndexBuffer& ib,
                    const ShaderProgram& program) const
{
    program.bind();
    va.bind();
    ib.bind();

    unsigned gl_primitive = get_gl_primitive();
    glDrawElements(gl_primitive, ib.get_count(), GL_UNSIGNED_INT, nullptr);
}

void Renderer::clear() const
{
    glClear(GL_COLOR_BUFFER_BIT); // Clear the screen.
    glClearColor(0.99f, 0.99f, 0.99f, 0.99f);
}

void Renderer::set_dark_background() const
{
    glClear(GL_COLOR_BUFFER_BIT); // Clear the screen.
    glClearColor(0.1f, 0.1f, 0.1f, 0.1f);
}

void Renderer::set_primitive(RenderPrimitive primitive)
{
    this->primitive = primitive;
}

unsigned Renderer::get_gl_primitive() const
{
    switch (primitive) {
        case RenderPrimitive::POINT: return GL_POINTS;
        case RenderPrimitive::LINE: return GL_LINES;
        case RenderPrimitive::TRIANGLE: return GL_TRIANGLES;
        case RenderPrimitive::TRIANGLE_FAN: return GL_TRIANGLE_FAN;
        default: throw std::runtime_error("Primitive not supported!");
    }
}
