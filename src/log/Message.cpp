#include "log/Message.hpp"

#include <sstream>
#include <vector>


static void indent(size_t n, std::ostream& s);

namespace Log {

Message::Message(const std::string& content,
        const std::string& preamble,
        ColorCode color_code)
    : content(content), preamble(preamble), mod(color_code) {}

Message::~Message() {}


std::ostream& operator<< (std::ostream& s, const Message& m)
{
    s << '[' << m.mod << m.preamble 
             << ColorModifier(ColorCode::FG_DEFAULT)
             << "] ";

    std::stringstream ss(m.content);
    std::string line;
    bool first_line = true;
    size_t pc = m.preamble.size() + 3;       // Preamble space consumed.
    while(std::getline(ss, line,'\n')) {
        if (Message::chars_per_line == 0u) { // No character per line.
            if (!first_line) indent(pc, s);
            s << line;
        } else { // Split message just word character limit is reached.
            size_t cs = Message::chars_per_line - pc;   // Content space left.

            size_t beg_i = 0;                           // Begin index.
            size_t end_i;                               // End index.
            std::string_view sv(line); 
            while (sv.size() > cs) {
                std::string_view w(sv.data(), cs);      // Working view.
                end_i = w.find_last_of(' ');            // Last space index.

                std::string_view svtp(w.data(), end_i); // View until space.
                if (!first_line) indent(pc, s);
                s << svtp << '\n';                      // Print until space.
                first_line = false;
                
                beg_i = end_i + 1; // Move beginning to end, but skip the space.
                sv = std::string_view(w.data() + beg_i); // New view.
            }
            if (!first_line) indent(pc, s);
            s << sv << '\n';
        }
        first_line = false;
    }
    return s; 
}

Log::Error::Error(const std::string& content)
    : Message(content, error_preamble, ColorCode::FG_RED) {}

Log::Warning::Warning(const std::string& content)
    : Message(content, warning_preamble, ColorCode::FG_YELLOW) {}

Log::Success::Success(const std::string& content)
    : Message(content, success_preamble, ColorCode::FG_GREEN) {}

Log::Note::Note(const std::string& content)
    : Message(content, note_preamble, ColorCode::FG_BLUE) {}

}

static void indent(size_t n, std::ostream& s)
{
    for (size_t i = 0; i < n; i++) s << ' '; // Print indentation.
} 
