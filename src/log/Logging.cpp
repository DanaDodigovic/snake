#include "log/Logging.hpp"

#include <iostream>

#include "log/Message.hpp"


namespace Log {

constexpr std::ostream& log_stream = std::cout;


void report_error(const std::exception& err)
{
    log_stream << Log::Error(err.what());        
}


void report_warning(const std::string& war)
{
    log_stream << Log::Warning(war);        
}


void report_success(const std::string& suc)
{
    log_stream << Log::Success(suc);
}


void report_note(const std::string& note)
{
    log_stream << Log::Note(note);
}

}
