#include "Control.hpp"
#include "State.hpp"

#include <algorithm>
#include <exception>
#include <memory>
#include <stdexcept>
#include <unordered_set>

#include <iostream>

namespace {
std::deque<Key> pressed_keys;

void keyboard_callback(
    GLFWwindow* window, int key, int scancode, int action, int mods);
} // namespace

KeyboardControl::KeyboardControl(const Window& w, Key start)
{
    w.set_keyboard_callback(keyboard_callback);
    pressed_keys.push_back(start);
}

Key KeyboardControl::get_key()
{
    if (pressed_keys.empty()) return Key::None;
    Key current_key = pressed_keys.front();
    pressed_keys.pop_front();
    return current_key;
}

namespace {
void keyboard_callback(
    GLFWwindow* /*window*/, int key, int /* scancode*/, int action, int
    /*mods*/)
{
    Key key_code;
    if (action == GLFW_PRESS) {
        key_code = openGL_to_key(key);
        if (key_code != Key::None) { pressed_keys.push_back(key_code); }
    }
}
} // namespace

SearchAlgorithmControl::SearchAlgorithmControl(const Snake& snake,
                                               const Food& food,
                                               const Window& window)
    : snake(snake), food(food), window(window)
{}

Key SearchAlgorithmControl::get_key()
{
    // Fill deque with keys.
    if (keys.empty()) {
        std::optional<Node> goal_node = search({snake.get_head(), 0, nullptr},
                                               {food.get_state(), 0, nullptr});
        if (goal_node) {
            const std::deque<State> rec_path = reconstruct_path(*goal_node);
            keys                             = from_states_to_keys(rec_path);
        }
        closed.clear(); // Clear for next search call.
    }

    // Fetch first key from deque.
    Key current_key = keys.front();
    keys.pop_front();
    return current_key;
}

std::optional<Node> SearchAlgorithmControl::search(const Node& start_node,
                                                   const Node& goal_node)
{
    std::unique_ptr<OpenNodesCollection> open =
        get_empty_open_nodes_collection();

    open->insert(start_node);

    std::unordered_set<State> visited; // For loop detection.

    while (!open->empty()) {
        Node node = open->remove_head();
        closed.push_back(node);

        if (node.state == goal_node.state) {
            open->clear();                    // Clear for next search call.
            return closed[closed.size() - 1]; // Return current node.
        }

        std::vector<Node> successors =
            get_successors(closed[closed.size() - 1]);
        visited.insert(node.state);

        for (auto&& m : successors) {
            if (!visited.count(m.state)) { open->insert(m); }
        }
    }

    return std::optional<Node>();
}

std::vector<Node> SearchAlgorithmControl::get_successors(const Node& node) const
{
    auto [x, y] = node.state.position;
    std::vector<Node> successors;
    Node succ_l(State(Position(x, y - 1)), node.depth + 1, &node);
    Node succ_r(State(Position(x, y + 1)), node.depth + 1, &node);
    Node succ_u(State(Position(x - 1, y)), node.depth + 1, &node);
    Node succ_d(State(Position(x + 1, y)), node.depth + 1, &node);

    if (succ_r.state == snake.get_tail()) {
        if (!is_node_border(succ_r)) { successors.push_back(succ_r); }
    } else {
        if (is_node_vaild(succ_r)) { successors.push_back(succ_r); }
    }
    if (succ_l.state == snake.get_tail()) {
        if (!is_node_border(succ_l)) { successors.push_back(succ_l); }
    } else {
        if (is_node_vaild(succ_l)) { successors.push_back(succ_l); }
    }
    if (succ_u.state == snake.get_tail()) {
        if (!is_node_border(succ_u)) { successors.push_back(succ_u); }
    } else {
        if (is_node_vaild(succ_u)) { successors.push_back(succ_u); }
    }
    if (succ_d.state == snake.get_tail()) {
        if (!is_node_border(succ_d)) { successors.push_back(succ_d); }
    } else {
        if (is_node_vaild(succ_d)) { successors.push_back(succ_d); }
    }

    return successors;
}

bool SearchAlgorithmControl::is_goal(const State& state)
{
    return state == food.get_state();
}

std::deque<State>
SearchAlgorithmControl::reconstruct_path(const Node& node) const
{
    std::deque<State> states;
    Node n = node;

    while (n.parent != nullptr) {
        states.push_front(n.state);
        n = *n.parent;
    }
    states.push_front(n.state);

    return states;
}

std::deque<Node> SearchAlgorithmControl::reconstruct_nodes(const Node& node)
{
    std::deque<Node> nodes;
    Node n = node;

    while (n.parent != nullptr) {
        nodes.push_front(n);
        n = *n.parent;
    }
    nodes.push_front(n);

    return nodes;
}

bool SearchAlgorithmControl::is_node_vaild(const Node& node) const
{
    return !(snake.contains(node.state) ||
             node.state.is_border(window.rows, window.cols));
}

BFSControl::BFSControl(const Snake& snake,
                       const Food& food,
                       const Window& window)
    : SearchAlgorithmControl(snake, food, window)
{}

std::deque<Key>
SearchAlgorithmControl::from_states_to_keys(const std::deque<State>& rec_path)
{
    std::deque<Key> new_keys;

    for (std::size_t i = 1; i < rec_path.size(); i++) {
        auto [prev_x, prev_y] = rec_path[i - 1].position;
        auto [curr_x, curr_y] = rec_path[i].position;

        if (curr_x > prev_x) new_keys.push_back(Key::Down);
        if (curr_x < prev_x) new_keys.push_back(Key::Up);
        if (curr_y > prev_y) new_keys.push_back(Key::Right);
        if (curr_y < prev_y) new_keys.push_back(Key::Left);
    }
    return new_keys;
}

DFSControl::DFSControl(const Snake& snake,
                       const Food& food,
                       const Window& window)
    : SearchAlgorithmControl(snake, food, window)
{}

void DFSControl::DFSOpenNodesCollection::insert(const Node& node)
{
    open.push(node);
}

bool DFSControl::DFSOpenNodesCollection::empty()
{
    return open.empty();
}

Node DFSControl::DFSOpenNodesCollection::remove_head()
{
    Node node = open.top();
    open.pop();
    return node;
}

void DFSControl::DFSOpenNodesCollection::clear()
{
    while (!open.empty()) open.pop();
}

std::unique_ptr<SearchAlgorithmControl::OpenNodesCollection>
DFSControl::get_empty_open_nodes_collection() const
{
    return std::make_unique<DFSOpenNodesCollection>();
}

LongestPathControl::LongestPathControl(const Snake& snake,
                                       const Food& food,
                                       const Window& window)
    : SearchAlgorithmControl(snake, food, window),
      shortest_path_control(snake, food, window)
{}

std::optional<Node> LongestPathControl::search(const Node& start_node,
                                               const Node& goal_node)
{
    std::optional<Node> goal =
        shortest_path_control.search(start_node, goal_node);
    // Get shortest path from start to goal node (so that you can expand it).
    if (goal)
        closed = shortest_path_control.reconstruct_nodes(*goal);
    else {
        return std::optional<Node>();
    }

    unsigned i = 0; // Index of element that will be expanded.
    bool found = false;
    while (i < closed.size() - 1) {
        std::vector<Node> first_succs = get_successors(closed[i]);
        for (auto& first_succ : first_succs) {
            std::vector<Node> second_succs = get_successors(first_succ);
            // Check if first_succ is already included in longest path.
            if (contains_state(first_succ, closed)) continue;
            for (auto& second_succ : second_succs) {
                // Check if second_succ is already included in longest path.
                if (contains_state(second_succ, closed)) continue;
                std::vector<Node> test_succs = get_successors(second_succ);
                // Check if you can go from current state (second_succ) to next
                // state in longest path (path should remain unbroken).
                if (!contains_state(closed[i + 1], test_succs)) continue;
                // Insert found nodes in path.
                closed.insert(closed.begin() + i + 1, first_succ);
                closed.insert(closed.begin() + i + 2, second_succ);
                // Longer path is found.
                found = true;
                break;
            }
            if (found) break;
        }

        if (!found) { // Move to the next pair of nodes.
            ++i;
        } else { // Check if there is still some longer path here.
            found = false;
        }
    }

    // Change parent nodes.
    for (size_t j = 0; j < closed.size(); ++j) {
        if (j > 0) closed[j].parent = &closed[j - 1];
    }
    return closed[closed.size() - 1];
}

std::vector<Node>
SearchAlgorithmControl::get_all_successors(const Node& node) const
{
    auto [x, y] = node.state.position;
    std::vector<Node> successors;
    Node succ_l(State(Position(x, y - 1)), node.depth + 1, &node);
    Node succ_r(State(Position(x, y + 1)), node.depth + 1, &node);
    Node succ_u(State(Position(x - 1, y)), node.depth + 1, &node);
    Node succ_d(State(Position(x + 1, y)), node.depth + 1, &node);

    if (!is_node_border(succ_r)) { successors.push_back(succ_r); }
    if (!is_node_border(succ_l)) { successors.push_back(succ_l); }
    if (!is_node_border(succ_u)) { successors.push_back(succ_u); }
    if (!is_node_border(succ_d)) { successors.push_back(succ_d); }

    return successors;
}

bool SearchAlgorithmControl::is_node_border(const Node& node) const
{
    return node.state.is_border(window.rows, window.cols);
}

bool LongestPathControl::contains_state(const Node& check_node,
                                        const std::deque<Node>& nodes)
{
    for (const auto& node : nodes) {
        if (check_node.state == node.state) return true;
    }
    return false;
}

bool LongestPathControl::contains_state(const Node& check_node,
                                        const std::vector<Node>& nodes)
{
    for (const auto& node : nodes) {
        if (check_node.state == node.state) return true;
    }
    return false;
}

void SearchAlgorithmControl::OpenNodesCollection::insert(const Node& node)
{
    open.push_back(node);
}

bool SearchAlgorithmControl::OpenNodesCollection::empty()
{
    return open.empty();
}

Node SearchAlgorithmControl::OpenNodesCollection::remove_head()
{
    Node node = open.front();
    open.pop_front();
    return node;
}

void SearchAlgorithmControl::OpenNodesCollection::clear()
{
    open.clear();
}

std::unique_ptr<SearchAlgorithmControl::OpenNodesCollection>
SearchAlgorithmControl::get_empty_open_nodes_collection() const
{
    return std::make_unique<OpenNodesCollection>();
}

Key HamiltonianPathControl::get_key()
{
    // Fill deque with keys.
    if (keys.empty()) {
        static int n_food_not_found   = -1; // Detect infinite loop.
        std::optional<Node> goal_node = search({snake.get_head(), 0, nullptr},
                                               {snake.get_tail(), 0, nullptr});
        if (goal_node) {
            const std::deque<State> rec_path = reconstruct_path(*goal_node);
            // Check if goal node is in reconstructed path.
            for (auto s : rec_path) {
                if (food.get_state() == s) {
                    n_food_not_found = -1;
                    break;
                }
            }
            // If it was not in last 5 iterations, return random key.
            if (++n_food_not_found == 10) {
                --n_food_not_found;
                return Key::None;
            }
            keys                  = from_states_to_keys(rec_path);
            std::deque<Key> keys2 = keys;
        }

        closed.clear(); // Clear for next search call.
    }

    // Fetch first key from deque.
    Key current_key = keys.front();
    keys.pop_front();
    return current_key;
}

HamiltonianPathControl::HamiltonianPathControl(const Snake& snake,
                                               const Food& food,
                                               const Window& window)
    : LongestPathControl(snake, food, window)
{}
