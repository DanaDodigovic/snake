#include "Game.hpp"

#include "imgui.h"
#include "examples/imgui_impl_glfw.h"
#include "examples/imgui_impl_opengl3.h"

#include <iostream>
#include <memory>
#include <chrono>
#include <thread>

Game::Game(Window& window, Configuration& config, DrawSettings& ds)
    : window(window),
      snake(food, ds.proj, ds.view, ds.program),
      food(ds.proj, ds.view, ds.program),
      grid(window.rows, window.cols, ds.proj, ds.view, ds.program),
      border(window.rows, window.cols, ds.proj, ds.view, ds.program),
      config(config),
      max_score((window.rows - 2) * (window.cols - 2) - 2),
      ds(ds)
{
    switch (config.control_type) {
        case (Control::Type::Keyboard):
            control = std::make_unique<KeyboardControl>(window, Key::Left);
            break;
        case (Control::Type::BFS):
            control = std::make_unique<BFSControl>(snake, food, window);
            break;
        case (Control::Type::DFS):
            control = std::make_unique<DFSControl>(snake, food, window);
            break;
        case (Control::Type::LongestPath):
            control = std::make_unique<LongestPathControl>(snake, food, window);
            break;
        case (Control::Type::Hamilton):
            control =
                std::make_unique<HamiltonianPathControl>(snake, food, window);
            break;
    }
}

void Game::start()
{
    while (!is_over() && window.open()) {
        // Calculate time taken for one loop run.
        Timepoint current_timepoint = Timestep::get_current_timepoint();
        Timestep timestep           = current_timepoint - last_frame_timepoint;
        last_frame_timepoint        = current_timepoint;

        render();
        update_game_state(timestep);
        if (score == max_score) render();
        window.poll_inputs();
        window.swap_buffers();
    }
    std::this_thread::sleep_for(std::chrono::seconds(1));
}

void Game::update_game_state(Timestep timestep)
{
    constexpr float game_speed = 0.006;
    time_accumulator += timestep;

    if (time_accumulator >= game_speed) {
        time_accumulator -= game_speed;
        if (!pause) {
            set_direction(snake.get_direction());
            snake.update_head(); // Move head to the next cell.

            if (snake.is_head_on(food)) {
                ++score;
                food.generate(snake, window.rows, window.cols);
            } else {
                snake.pop_tail(); // Remove tail from the last cell.
            }
            snake.update_buffers(); // Update buffers for next render call.
        } else if (pause) {
            set_direction(snake.get_direction());
        }
    }
}

bool Game::is_over() const
{
    return snake.is_head_on_border(window.rows, window.cols) ||
           snake.is_head_on_body() || score == max_score;
}

void Game::set_direction(Direction current_direction)
{
    switch (control->get_key()) {
        case Key::Left:
            if (current_direction != Direction::Right && !pause) {
                snake.change_direction(Direction::Left);
            }
            break;
        case Key::Right:
            if (current_direction != Direction::Left && !pause) {
                snake.change_direction(Direction::Right);
            }
            break;
        case Key::Up:
            if (current_direction != Direction::Down && !pause) {
                snake.change_direction(Direction::Up);
            }
            break;
        case Key::Down:
            if (current_direction != Direction::Up && !pause) {
                snake.change_direction(Direction::Down);
            }
            break;
        case Key::Pause: pause = pause ? false : true; break;
        default: break; // Direction reamins the same.
    }
}

void Game::render()
{
    renderer.clear();
    grid.draw(renderer);
    border.draw(renderer);
    food.draw(renderer);
    snake.draw(renderer);

    // IMGUI ---------------------------------------- BEGIN
    ImGui_ImplOpenGL3_NewFrame();
    ImGui_ImplGlfw_NewFrame();
    ImGui::NewFrame();
    // IMGUI =========================================

    constexpr float distance_h = 56.0f;
    constexpr float distance_w = 70.0f;
    auto [width, height]       = window.dimensions();
    ImGui::SetNextWindowPos({width - distance_w, height - distance_h},
                            ImGuiCond_Always);
    ImGui::Begin("Score",
                 nullptr,
                 ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoDecoration |
                     ImGuiWindowFlags_AlwaysAutoResize |
                     ImGuiWindowFlags_NoSavedSettings |
                     ImGuiWindowFlags_NoFocusOnAppearing |
                     ImGuiWindowFlags_NoNav | ImGuiWindowFlags_NoBackground);
    ImGui::Text("%d", score);
    ImGui::End();

    // IMGUI ---------------------------------------- END
    // Set display size.
    ImGuiIO& io = ImGui::GetIO();
    io.DisplaySize =
        ImVec2(window.dimensions().first, window.dimensions().second);

    // Render ImGui to the screen.
    ImGui::Render();
    ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
    // IMGUI =========================================
}

unsigned Game::get_score() const
{
    return score;
}
