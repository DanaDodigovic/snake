#include "Timestep.hpp"
#include <chrono>

Timestep::Timestep(Duration time) : time(time) {}

std::chrono::seconds Timestep::get_seconds() const
{
    return std::chrono::duration_cast<std::chrono::seconds>(time);
}

std::chrono::milliseconds Timestep::get_milliseconds() const
{
    return std::chrono::duration_cast<std::chrono::milliseconds>(time);
}

std::chrono::nanoseconds Timestep::get_nanoseconds() const
{
    return std::chrono::duration_cast<std::chrono::milliseconds>(time);
}

Timestep::operator float() const
{
    constexpr auto nanoseconds_per_second = 1e9f;
    return get_nanoseconds().count() / nanoseconds_per_second;
    //return get_seconds().count();
}

Timepoint Timestep::get_current_timepoint()
{
    return TimestepClock::now();
}
