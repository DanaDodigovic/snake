#include "Node.hpp"

Node::Node(const State& state, int depth, const Node* parent)
    : state(state), depth(depth), parent(parent)
{}

bool operator==(const Node& lhs, const Node& rhs)
{
    return lhs.state == rhs.state && lhs.parent == rhs.parent &&
           lhs.depth == rhs.depth;
}

std::ostream& operator<<(std::ostream& os, const Node& node)
{
    os << '[' << node.state << ", ";
    if (node.parent) {
         os << node.parent->state;
    } else {
        os << 'x';
    }
        return  os << ", " << node.depth << ']';
}
