#include "Inputs.hpp"

#include <GLFW/glfw3.h>

#include <iostream>

Key openGL_to_key(int key_code)
{
    switch (key_code) {
        case GLFW_KEY_LEFT: return Key::Left;
        case GLFW_KEY_RIGHT: return Key::Right;
        case GLFW_KEY_UP: return Key::Up;
        case GLFW_KEY_DOWN: return Key::Down;
        case GLFW_KEY_SPACE: return Key::Pause;
        default: return Key::None;
    }
}
