#include "Window.hpp"

#include <chrono>
#include <thread>

#include "imgui.h"
#include "examples/imgui_impl_glfw.h"
#include "examples/imgui_impl_opengl3.h"

#ifndef NDEBUG
    #include "Errors.hpp"
#endif

Window::Window(unsigned rows,
               unsigned cols,
               unsigned width,
               unsigned height,
               const std::string& title,
               bool enable_vsync)
    : rows(rows), cols(cols), enable_vsync(enable_vsync)
{
    if (!glfwInit()) throw std::runtime_error("Failed to initialize GLFW.");

    glfwWindowHint(GLFW_RESIZABLE, GL_FALSE); // Make window fixed size.
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 6);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE); // Use core.

    // Create a windowed mode window of provided size.
    window = glfwCreateWindow(width, height, title.c_str(), nullptr, nullptr);
    if (!window) throw std::runtime_error("Failed to create the window.");

    glfwMakeContextCurrent(window); // Enable OpenGL drawing to this window.

    // Enable or disable VSync
    enable_vsync ? glfwSwapInterval(1) : glfwSwapInterval(0);

    // Initialize modern OpenGL - pull functions from GPU drivers.
    if (glewInit() != GLEW_OK) {
        throw std::runtime_error("Failed to initialize GLEW.");
    }

#ifndef NDEBUG
    Errors::enable();
#endif

    // IMGUI ----------------------------------------
    // Setup ImGui context.
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGuiIO& io = ImGui::GetIO();
    io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard; // Enable keyboard.
    float font_size = dimensions().first / rows - 20;
    io.Fonts->AddFontFromFileTTF("res/fonts/Hack-Regular.ttf", font_size);

    ImGui::StyleColorsDark();

    ImGuiStyle& style = ImGui::GetStyle();
    if (io.ConfigFlags) {
        style.WindowRounding              = 0.0f;
        style.Colors[ImGuiCol_WindowBg].w = 1.0f;
    }
    ImGui::PushStyleColor(ImGuiCol_WindowBg, 1);
    style.Colors[ImGuiCol_WindowBg].w = 0.0f;

    ImGui_ImplGlfw_InitForOpenGL(window, true);
    ImGui_ImplOpenGL3_Init("#version 410");
    // IMGUI =========================================
}

Window::~Window()
{
    // IMGUI ----------------------------------------
    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplGlfw_Shutdown();
    ImGui::DestroyContext();
    // IMGUI =========================================

    //std::this_thread::sleep_for(std::chrono::seconds(7)); // TODO: Remove!
    glfwDestroyWindow(window);
    glfwTerminate();
}

unsigned Window::get_rows() const
{
    return rows;
}

unsigned Window::get_cols() const
{
    return cols;
}

bool Window::open() const
{
    return !glfwWindowShouldClose(window);
}

void Window::swap_buffers()
{
    glfwSwapBuffers(window); // Swap front and back buffer.
}

void Window::poll_inputs()
{
    glfwPollEvents(); // Poll for events and process them.
}

std::pair<unsigned, unsigned> Window::dimensions() const
{
    int width, height;
    glfwGetWindowSize(window, &width, &height);
    return {width, height};
}

void Window::set_keyboard_callback(GLFWkeyfun callback) const
{
    glfwSetKeyCallback(window, callback);
}
