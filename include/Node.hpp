#ifndef NODE_HPP
#define NODE_HPP

#include "State.hpp"

#include <algorithm>
#include <memory>

class Node {
  public:
    State state;
    int depth;
    const Node* parent;

  public:
    Node(const State& state, int depth, const Node* parent);
    friend bool operator==(const Node& lhs, const Node& rhs);
    friend std::ostream& operator<<(std::ostream& os, const Node& node);
};

#endif
