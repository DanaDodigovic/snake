#ifndef INDEX_BUFFER_HPP
#define INDEX_BUFFER_HPP

class IndexBuffer {
  private:
    unsigned id;
    unsigned count;

  public:
    IndexBuffer(const unsigned* data, unsigned count);
    ~IndexBuffer();
    IndexBuffer(const IndexBuffer& other) = delete;
    IndexBuffer& operator=(const IndexBuffer& other) = delete;

    void bind() const;
    void unbind() const;
    void set_indices(const unsigned* data, unsigned count);
    inline unsigned get_count() const { return count; }

};

#endif
