#ifndef SNAKE_HPP
#define SNAKE_HPP

#include "Food.hpp"

enum class Direction { Left, Right, Up, Down };

class Snake : public RenderObject {
  private:
    Food& food;
    std::vector<State> states;
    Direction direction = Direction::Left;

  public:
    Snake(Food& food,
          const glm::mat4& proj,
          const glm::mat4& view,
          ShaderProgram& program);

    void update_buffers();
    void pop_tail();
    void update_head();

    bool is_head_on(const Food& food) const;
    bool is_head_on_border(unsigned rows, unsigned cols) const;
    bool is_head_on_body() const;
    bool contains(const State& state) const;

    void change_direction(Direction direction);
    Direction get_direction() const;
    const State& get_head() const;
    const State& get_tail() const;
    
    const std::vector<State>& get_body_parts() const;


  protected:
    void set_vertices() override;
    void set_indices() override;

  private:
    void
    set_cell_vertices(float x, float y, const glm::vec3 color, size_t index);
};

#endif
