#ifndef STATE_HPP
#define STATE_HPP

#include "Position.hpp"

#include <functional>
#include <ostream>

class State;

namespace std {
template<>
struct hash<State> {
    size_t operator()(const State& state) const;
};
}

/** Represents one cell on board. */
class State {
  public:
    Position position;
    // Color color;
    // enum PositionType: lu corner, vertical, hor...

  public:
    State() = default;
    State(Position position);

    void set_position(Position position);
    bool is_border(int rows, int cols) const;

    friend bool operator==(const State& first, const State& second);
    friend std::ostream& operator<<(std::ostream& os, const State& state);
};


#endif
