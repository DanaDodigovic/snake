#ifndef CONFIGUARTION_HPP
#define CONFIGUARTION_HPP

#include "Control.hpp"

class Configuration {
  public:
    Control::Type control_type;

  public:
    Configuration() = default;
    Configuration(Control::Type control_type);
};

#endif
