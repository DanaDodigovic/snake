#ifndef WINDOW_HPP
#define WINDOW_HPP

#include "Snake.hpp"
#include "Inputs.hpp"

#include <GL/glew.h>
#include <GLFW/glfw3.h>

class Window {
  public:
    const unsigned rows, cols;

  private:
    GLFWwindow* window;
    bool enable_vsync = false;

  public:
    Window(unsigned rows,
           unsigned cols,
           unsigned width,
           unsigned height,
           const std::string& title,
           bool enable_vsync);
    ~Window();

    bool open() const;

    void swap_buffers();
    void poll_inputs();
    void set_keyboard_callback(GLFWkeyfun callback) const;

    unsigned get_rows() const;
    unsigned get_cols() const;

    std::pair<unsigned, unsigned> dimensions() const;
};

#endif
