#ifndef GRID_HPP
#define GRID_HPP

#include "RenderObject.hpp"
#include "State.hpp"

#include <vector>

class Grid : public RenderObject {
  private:
    unsigned rows, cols;

  public:
    Grid(unsigned rows,
         unsigned cols,
         const glm::mat4& proj,
         const glm::mat4& view,
         ShaderProgram& program);

  private:
    void set_vertices() override;
    void set_indices() override;

};

#endif
